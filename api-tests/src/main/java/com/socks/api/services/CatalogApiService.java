package com.socks.api.services;

import com.socks.api.assertions.AssertableResponse;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CatalogApiService {

    RequestSpecification requestSpec;
    public final String defaultContentType = "application/json; charset=utf-8";

    public CatalogApiService(String basePath) {
        this.requestSpec = RestAssured.given()
                .relaxedHTTPSValidation()
                .contentType(defaultContentType)
                .filters(new RequestLoggingFilter(), new ResponseLoggingFilter(), new AllureRestAssured())
                .basePath(basePath);
    }

    public AssertableResponse getCatalog() {
        log.info("GET all catalog");
        return new AssertableResponse(requestSpec.when()
                .get("/")
                .then());
    }

    public AssertableResponse getCatalogItem(String id) {
        log.info("GET catalog id {}", id);
        return new AssertableResponse(requestSpec.when()
                .get("/" + id)
                .then());
    }

    public AssertableResponse getCatalogSize() {
        log.info("GET catalog size");
        return new AssertableResponse(requestSpec.when()
                .get("/size")
                .then());
    }

    public AssertableResponse getTags() {
        log.info("GET tags");
        return new AssertableResponse(requestSpec.when()
                .get("/tags")
                .then());
    }

}
