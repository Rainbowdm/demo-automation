package com.socks.api.services;

import com.socks.api.assertions.AssertableResponse;
import com.socks.api.model.Address;
import com.socks.api.model.Card;
import com.socks.api.model.User;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.List;
import java.util.Map;

import static com.socks.api.utils.PropertyData.putValueToFile;

@Slf4j
public class UserApiService {

    RequestSpecification requestSpec;
    public final String defaultContentType = "application/json; charset=utf-8";

    public UserApiService(String basePath) {
        this.requestSpec = RestAssured.given()
                .relaxedHTTPSValidation()
                .contentType(defaultContentType)
//                .auth().preemptive().basic(username, password)
                .filters(new RequestLoggingFilter(), new ResponseLoggingFilter(), new AllureRestAssured())
                .basePath(basePath);
    }

    public void generateUserNameAndPassword() {
        String username = RandomStringUtils.randomAlphabetic(6);
        putValueToFile("username", username, "register", "Register fields API");
    }

    public void setupBasicAuth(String username, String password) {
        PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
        authScheme.setUserName(username);
        authScheme.setPassword(password);
        RestAssured.authentication = authScheme;
        log.info("Basic auth " + username + " " + password);
    }

    public AssertableResponse registerUser(User user) {
        log.info("Register user {}", user);
        return new AssertableResponse(requestSpec.when()
                .body(user)
                .when()
                .post("register")
                .then());
    }

    public AssertableResponse getCustomers() {
        log.info("GET customers");
        return new AssertableResponse(requestSpec.when()
                .get("customers")
                .then());
    }

    public AssertableResponse getCustomer(String id){
        log.info("GET customer id {}", id);
        return new AssertableResponse(requestSpec.when()
                .get("customers/" + id)
                .then());
    }

    public AssertableResponse deleteCustomer(String id) {
        log.info("DELETE customer id {}", id);
        return new AssertableResponse(requestSpec.when()
                .delete("customers/" + id)
                .then());
    }

    public void deleteCustomerWithName(String userName){
        List<Map> customers = getCustomers().response.extract().body()
                .jsonPath().param("userName", userName).get("_embedded.customer.findAll { customer -> customer.username = userName }");
        customers.forEach((customer)-> deleteCustomer(customer.get("id").toString()));
    }

    public AssertableResponse loginUser(User user, String username, String password) {
        log.info("Login user {}", user);
        log.info("Basic auth " + username + " " + password);
        return new AssertableResponse(requestSpec.when()
                .auth().preemptive().basic(username, password)
                .body(user)
                .when()
                .get("login")
                .then());
    }

    public AssertableResponse getCards(){
        log.info("GET cards");
        return new AssertableResponse(requestSpec.when()
                .get("/cards")
                .then());
    }

    public AssertableResponse getCard(String cardId){
        log.info("GET card id {}", cardId);
        return new AssertableResponse(requestSpec.when()
                .get("/cards/" + cardId)
                .then());
    }

    public AssertableResponse addCard(Card card){
        log.info("Add card {}", card);
        return new AssertableResponse(requestSpec.when()
                .body(card)
                .post("/cards")
                .then());
    }

    public AssertableResponse deleteCard(String cardId){
        log.info("DELETE card id {}", cardId);
        return new AssertableResponse(requestSpec.when()
                .delete("/cards/" + cardId)
                .then());
    }

    public AssertableResponse getAddresses() {
        log.info("GET added address");
        return new AssertableResponse(requestSpec.when()
                .get("/addresses")
                .then());
    }

    public AssertableResponse getAddress(String id) {
        log.info("GET address id {}", id);
        return new AssertableResponse(requestSpec.when()
                .get("/addresses/" + id)
                .then());
    }

    public AssertableResponse addAddress(Address address) {
        log.info("Add address {}", address);
        return new AssertableResponse(requestSpec.when()
                .body(address)
                .post("/addresses")
                .then());
    }

    public AssertableResponse deleteAddress(String id) {
        log.info("DELETE address id {}", id);
        return new AssertableResponse(requestSpec.when()
                .delete("/addresses/" + id)
                .then());
    }
}