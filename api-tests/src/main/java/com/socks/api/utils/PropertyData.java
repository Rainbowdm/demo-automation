package com.socks.api.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

@Slf4j
public class PropertyData {

    private static Properties properties;
    private static FileInputStream inputStream;
    private static FileOutputStream outputStream;

    private static final String pathToFile = "src/test/resources/pageProperties/";

    public static String getValueFromFile(String key, String fileName) {
        properties = new Properties();
        try {
            inputStream = new FileInputStream(pathToFile + fileName + ".properties");
            properties.load(inputStream);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(key);
    }

    public static void putValueToFile(String key, String value, String fileName, String description) {

        properties = new Properties();

        try {
            properties.load(new FileInputStream(pathToFile + fileName + ".properties"));
            properties.put(key, value);
            outputStream = new FileOutputStream(pathToFile + fileName + ".properties");
            properties.store(outputStream, description);
        } catch (Exception e) {
            log.error("Can't set properties in file" + e.getMessage());
        }
    }
}
