package com.socks.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.Accessors;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
public class Card {

    @JsonProperty("longNum")
    public String longNum;

    @JsonProperty("expires")
    public String expires;

    @JsonProperty("ccv")
    public String ccv;

    @JsonProperty("userID")
    public String userID;

    @JsonProperty("id")
    public String id;
}
