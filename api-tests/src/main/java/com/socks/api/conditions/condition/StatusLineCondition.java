package com.socks.api.conditions.condition;

import com.socks.api.conditions.Condition;
import io.restassured.response.ValidatableResponse;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class StatusLineCondition implements Condition {

    private String statusLine;

    @Override
    public void check(ValidatableResponse response) {
        response.assertThat().statusLine(statusLine);
    }

    @Override
    public String toString() {
        return "Status line is: " + statusLine;
    }
}
