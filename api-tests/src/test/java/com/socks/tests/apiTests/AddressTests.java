package com.socks.tests.apiTests;

import com.socks.api.model.Address;

import org.junit.jupiter.api.Test;

import java.util.List;

import static com.socks.api.conditions.Conditions.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddressTests extends TestBase {

    @Test
    void testAddNewAddress() {
        Address address = new Address().setUserID(testUser.id)
                .setCountry("Ukraine")
                .setCity("Odessa")
                .setStreet("Deribasovskaya")
                .setNumber("7").setPostcode("0512314");

        userApiService.addAddress(address)
                .shouldHave(statusCode(200))
                .shouldHave(contentType("application/json; charset=utf-8"))
                .shouldHave(body(matchesJsonSchemaInClasspath("responseJsonScheme/register.json")))
                .shouldHave(body("id", is(nullValue())));
    }

    //Update
    @Test
    void testGetAllAddresses() {
        List<Address> address = userApiService.getAddresses()
                .shouldHave(statusCode(200))
//                .shouldHave(contentType("application/json; charset=utf-8"))
                .shouldHave(body(matchesJsonSchemaInClasspath("responseJsonScheme/address.json")))
                .response.extract().body().jsonPath().get("_embedded.address");

        assertTrue(address.size() > 0);
        assertEquals(4, address.size());
        assertEquals("57a98d98e4b00679b4a830ad", address.get(0).userID);
    }

    @Test
    void testGetAddress() {
        Address address = userApiService.getAddress("57a98ddce4b00679b4a830d1")
                .shouldHave(statusCode(200))
                .shouldHave(contentType("application/json; charset=utf-8"))
                .shouldHave(body(matchesJsonSchemaInClasspath("responseJsonScheme/address.json")))
                .response.extract().body().as(Address.class);
        assertEquals("UK", address.country);
        assertEquals("London", address.city);
    }
}
