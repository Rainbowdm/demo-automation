package com.socks.tests.apiTests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.socks.api.model.Product;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static com.socks.api.conditions.Conditions.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class CatalogTests extends TestBase {

    @Test
    void testGetCatalogItemsWithMoreAssertions() {
        Product[] products = gson.fromJson(catalogApiService.getCatalog()
                .shouldHave(statusCode(200))
                .shouldHave(contentType("text/plain; charset=utf-8"))
                .shouldHave(body(matchesJsonSchemaInClasspath("responseJsonScheme/catalog.json")))
                .response.extract().body().asString(), Product[].class);

        assertTrue(products.length > 0);
        assertEquals(9, products.length);
        Product product = products[0];
        assertTrue(product.tag.size() > 1);
        assertEquals(testProduct.id, product.getId());
        assertEquals(testProduct.description, product.getDescription());
        assertEquals(testProduct.getPrice(), product.price);
    }

    @Test
    void testGetCatalogItems() throws IOException {
        List<Product> products = objectMapper.readValue(catalogApiService.getCatalog()
                .shouldHave(statusCode(200))
                .shouldHave(body(matchesJsonSchemaInClasspath("responseJsonScheme/catalog.json")))
                .response.extract().body().asString(), new TypeReference<List<Product>>() {
        });

        assertTrue(products.size() > 0);
        assertEquals(9, products.size());
        assertEquals("Holy", products.get(0).name);
        assertThat(products.size(), equalTo(9));
        assertThat(products.get(0).description, equalTo(testProduct.getDescription()));
        assertThat(products.get(0).id, is(testProduct.id));
    }

    @Test
    void testGetCatalogWithJacksonLibraryMapping() throws IOException {
        List<Product> products = objectMapper.readValue(catalogApiService.getCatalog()
                .shouldHave(statusCode(200))
                .shouldHave(body(matchesJsonSchemaInClasspath("responseJsonScheme/catalog.json")))
                .response.extract().body().asString(), new TypeReference<List<Product>>() {
        });

        assertEquals(9, products.size());
        assertTrue(products.get(0).tag.size() > 1);
        assertEquals(testProduct.id, products.get(0).getId());
        assertEquals(testProduct.description, products.get(0).getDescription());
        assertEquals(testProduct.getPrice(), products.get(0).price);
    }

    @Test
    void testGetProductWithInvalidContentType() {
        Throwable exception = assertThrows(AssertionError.class, () -> {
            catalogApiService.getCatalogItem(testProduct.getId())
                    .shouldHave(statusCode(200))
                    .shouldHave(contentType("application/json; charset=utf-8"))
                    .shouldHave(body("$[0].name", equalTo("Holy")));
        });
        assertEquals("1 expectation failed.\n" +
                        "Expected content-type \"application/json; charset=utf-8\" doesn't match actual content-type \"text/plain; charset=utf-8\".\n",
                exception.getMessage());
    }

    @Test
    void testGetProductWithGsonLibraryMapping() {
        Product product = gson.fromJson(catalogApiService.getCatalogItem(testProduct.getId())
                .shouldHave(statusCode(200))
                .shouldHave(body(matchesJsonSchemaInClasspath("responseJsonScheme/catalog.json")))
                .response.extract().response().body().asString(), Product.class);

        assertTrue(product.tag.size() > 1);
        assertEquals(testProduct.id, product.getId());
        assertEquals(testProduct.description, product.getDescription());
        assertEquals(testProduct.getPrice(), product.price);
    }

    @Test
    void testGetProductsWithJacksonLibraryMapping() throws IOException {
        List<Product> products = objectMapper.readValue(catalogApiService.getCatalogItem(testProduct.id)
                .shouldHave(statusCode(200))
                .shouldHave(body(matchesJsonSchemaInClasspath("responseJsonScheme/catalog.json")))
                .response.extract().body().asString(), new TypeReference<List<Product>>() {
        });

        assertEquals(9, products.size());
        assertTrue(products.get(0).tag.size() > 1);
        assertEquals(testProduct.id, products.get(0).getId());
        assertEquals(testProduct.description, products.get(0).getDescription());
        assertEquals(testProduct.getPrice(), products.get(0).price);
    }

    //
    @Test
    void testGetProductsWithGsonLibraryMapping() {
        Product[] products = gson.fromJson(catalogApiService.getCatalogItem(testProduct.id)
                .shouldHave(statusCode(200))
                .response.extract().body().asString(), Product[].class);
        assertAll("Validate tests product",
                () -> assertEquals(9, products.length),
                () -> assertTrue(products[0].tag.size() > 1),
                () -> assertEquals(testProduct.id, products[0].getId()),
                () -> assertEquals(testProduct.description, products[0].getDescription()),
                () -> assertEquals(testProduct.getPrice(), products[0].price)
        );
    }
}
