package com.socks.tests.apiTests;

import com.socks.api.model.Card;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.socks.api.conditions.Conditions.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CardsTests extends TestBase {

    @Test
    void testAddCardToUser() {
        Card card = new Card()
                .setLongNum("4444 4444 4444 4444")
                .setExpires("19/02")
                .setCcv("123")
                .setUserID(testUser.id);

        userApiService.addCard(card)
                .shouldHave(statusCode(200))
                .shouldHave(contentType("text/plain; charset=utf-8"))
                .shouldHave(body("id", is(notNullValue())));
    }

    //Update
    @Test
    void testGetAllCards() {
        List<Card> cards = userApiService.getCards()
                .shouldHave(statusCode(200))
                .shouldHave(contentType("text/plain; charset=utf-8"))
//                .shouldHave(body("_embedded.card.size()", is(4)))
                .shouldHave(body(matchesJsonSchemaInClasspath("responseJsonScheme/card.json")))
                .response.extract().body().jsonPath().get("_embedded.card");

        assertTrue(cards.size() > 0);
        assertEquals(4, cards.size());
        assertEquals("5429804235432", cards.get(0).longNum);
        assertEquals("04/16", cards.get(0).expires);
        assertEquals("432", cards.get(0).ccv);
    }

    @Test
    void testGetAllCardsWithGsonMapping() {
        Card card = new Card()
                .setLongNum("5429804235432")
                .setExpires("04/16")
                .setCcv("432")
                .setId("57a98ddce4b00679b4a830d2");

        gson.fromJson(userApiService.getCards()
                .shouldHave(statusCode(200))
                .shouldHave(contentType("text/plain; charset=utf-8"))
                .shouldHave(body(matchesJsonSchemaInClasspath("responseJsonScheme/card.json")))
                .response.extract().body().asString(), Card.class);

        assertEquals("5429804235432", card.longNum);
        assertEquals("04/16", card.expires);
        assertEquals("432", card.ccv);
        assertEquals("57a98ddce4b00679b4a830d2", card.id);
    }
}
