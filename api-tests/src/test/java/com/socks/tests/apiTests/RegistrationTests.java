package com.socks.tests.apiTests;

import com.socks.api.assertions.AssertableResponse;
import com.socks.api.model.User;
import org.junit.jupiter.api.*;

import java.util.List;
import java.util.Map;

import static com.socks.api.conditions.Conditions.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RegistrationTests extends TestBase {

    public User usUser;

    @BeforeAll
    static void stepUpTest() {
        List<Map> customers = userApiService.getCustomers().response.extract().body()
                .jsonPath().param("userName", "test.test")
                .get("_embedded.customer.findAll()");
        for (int i = 0, customersSize = customers.size(); i < customersSize; i++) {
            Map customer = customers.get(i);
            userApiService.deleteCustomer(customer.get("id").toString());
        }
    }

    @AfterAll
    static void tearDownTests() {
        List<Map> customers = userApiService.getCustomers().response.extract().body()
                .jsonPath().param("userName", "test.test")
                .get("_embedded.customer.findAll { customer -> customer.username != userName }");
        for (Map customer : customers) {
            userApiService.deleteCustomer(customer.get("id").toString());
        }
    }

    @Test
    void testRegisterCustomerWithValidCredentials() {
        // given
        usUser = new User().setFirstName(usFaker.name().firstName())
                .setLastName(usFaker.name().lastName())
                .setUsername(usFaker.name().username())
                .setEmail(usFaker.internet().emailAddress())
                .setPassword(usFaker.internet().password())
                .setId(usFaker.internet().uuid());
        // expect
        userApiService.registerUser(usUser)
                .shouldHave(statusCode(200))
                .shouldHave(contentType("application/json; charset=utf-8"))
                .shouldHave(body("id", notNullValue()))
                .shouldHave(body("id", containsString("0")))
                .shouldHave(body(matchesJsonSchemaInClasspath("responseJsonScheme/register.json")));
    }

    @Test
    void testRegisterCustomerWithExistingAccount() {
        // given
        usUser = new User().setFirstName(usFaker.name().firstName())
                .setLastName(usFaker.name().lastName())
                .setUsername(usFaker.name().username())
                .setEmail(usFaker.internet().emailAddress())
                .setPassword(usFaker.internet().password())
                .setId(usFaker.internet().uuid());

        // expect
        userApiService.registerUser(usUser)
                .shouldHave(statusCode(200));
        AssertableResponse r = userApiService.registerUser(usUser)
                .shouldHave(statusCode(500))
                .shouldHave(contentType("text/plain; charset=utf-8"));
        assertEquals(500, r.response.extract().statusCode());
        assertEquals("text/plain; charset=utf-8", r.response.extract().contentType());
        assertEquals(1, userApiService.getCustomers().response.extract().body()
                .jsonPath().param("userName", usUser.username)
                .getList("_embedded.customer.findAll { customer -> customer.username == userName }").size());
    }

    @Test
    void testRegisterCustomerWithInvalidData() {
        // given
        User user = new User()
                .setFirstName("test")
                .setLastName("test")
                .setUsername(null)
                .setEmail("tests@@@mail.com")
                .setPassword("qwerty123");

        // expect
        userApiService.registerUser(user)
                .shouldHave(statusCode(500))
                .shouldHave(contentType("text/plain; charset=utf-8"))
                .shouldHave(body(is("")));
    }
}
