package com.socks.tests.apiTests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.socks.api.model.Product;
import com.socks.api.model.User;
import com.socks.api.services.CatalogApiService;
import com.socks.api.services.UserApiService;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;

import java.util.Locale;

public class TestBase {

    public static UserApiService userApiService;
    public static CatalogApiService catalogApiService;


    public static Faker usFaker;

    //Json mappers
    public static Gson gson = new Gson();
    public static ObjectMapper objectMapper = new ObjectMapper();

    public static Product testProduct;
    public static User testUser;


    @BeforeAll
    static void setUpServices() {
        RestAssured.baseURI = "http://127.0.0.1/";

        userApiService = new UserApiService("/");
        catalogApiService = new CatalogApiService("/catalogue");

        usFaker = new Faker(new Locale("en-US"));

        testProduct = new Product()
                .setName("Holly")
                .setId("03fef6ac-1896-4ce8-bd69-b798f85c6e0b")
                .setDescription("Socks fit for a Messiah. You too can experience walking in water with these special edition beauties. Each hole is lovingly proggled to leave smooth edges. The only sock approved by a higher power.")
                .setPrice(99.99)
                .setCount(1);
        testUser = new User()
                .setFirstName("")
                .setLastName("")
                .setUsername("test.test")
                .setId("5bb0acefee11cb00018b0de3")
                .setPassword("1234");
    }
}
