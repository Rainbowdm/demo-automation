package com.socks.tests.apiTests;

import com.socks.api.model.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static com.socks.api.conditions.Conditions.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.*;

public class LoginTests extends TestBase {

    public User usUser;

    @BeforeAll
    static void stepUpTest() {
        List<Map> customers = userApiService.getCustomers().response.extract().body()
                .jsonPath().param("userName", "test.test")
                .get("_embedded.customer.findAll()");
        for (int i = 0, customersSize = customers.size(); i < customersSize; i++) {
            Map customer = customers.get(i);
            userApiService.deleteCustomer(customer.get("id").toString());
        }
    }

    @BeforeEach
    void setUpTest() {
        usUser = new User().setFirstName(usFaker.name().firstName())
                .setLastName(usFaker.name().lastName())
                .setUsername(usFaker.name().username())
                .setEmail(usFaker.internet().emailAddress())
                .setPassword(usFaker.internet().password());

        userApiService.registerUser(usUser);
    }

    @AfterAll
    static void tearDownTests() {
        List<Map> customers = userApiService.getCustomers().response.extract().body()
                .jsonPath().param("userName", "test.test")
                .get("_embedded.customer.findAll { customer -> customer.username != userName }");
        for (Map customer : customers) {
            userApiService.deleteCustomer(customer.get("id").toString());
        }
    }

    @Test
    void testCanLoginUserWithValidCredentials() {
        userApiService.loginUser(usUser, usUser.username, usUser.password)
                .shouldHave(statusCode(200))
                .shouldHave(contentType("text/html; charset=utf-8"))
                .shouldHave(body("html.body", equalTo("Cookie is set")));
    }

    @Test
    void testCanLoginJustRegisteredUser() {
        userApiService.loginUser(usUser, usUser.username, usUser.password)
                .shouldHave(statusCode(200))
                .shouldHave(contentType("text/html; charset=utf-8"))
                .shouldHave(body("html.body", equalTo("Cookie is set")));

        userApiService.loginUser(usUser, usUser.username, usUser.password)
                .shouldHave(statusCode(200))
                .shouldHave(contentType("text/html; charset=utf-8"))
                .shouldHave(body("html.body", equalTo("Cookie is set")));
    }

    @Test
    void testCanNotLoginWithoutPassword() {
        userApiService.loginUser(usUser, usUser.username, "")
                .shouldHave(statusCode(401))
                .shouldHave(contentType("text/plain; charset=utf-8"))
                .shouldHave(statusLine("HTTP/1.1 401 Unauthorized"));
    }

    @Test
    void testCanNotLoginWithInvalidUsername() {
        userApiService.loginUser(usUser, "", usUser.password)
                .shouldHave(statusCode(401))
                .shouldHave(contentType("text/plain; charset=utf-8"))
                .shouldHave(statusLine("HTTP/1.1 401 Unauthorized"));
    }

    @Test
    void testCanNotLoginWithoutCredentials() {
        userApiService.loginUser(usUser, "", "")
                .shouldHave(statusCode(401))
                .shouldHave(contentType("text/plain; charset=utf-8"))
                .shouldHave(statusLine("HTTP/1.1 401 Unauthorized"));
    }

    @Test
    void testCheckLoginSchemeJson() {
        userApiService.getCustomers()
                .shouldHave(statusCode(200))
                .shouldHave(contentType("text/plain; charset=utf-8"))
                .shouldHave(body(matchesJsonSchemaInClasspath("responseJsonScheme/loginUser.json")));
    }
}
